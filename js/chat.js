$(
    function () {
        $("#btnConnect").click(function () {
            $("#messageSpan").text("Connection...");
            ws = new WebSocket("ws://http://localhost:56117/api/WSChat");
            ws.onopen = function () {
                $("#messageSpan").text("Connected!");
            };
            ws.onmessage = function (result) {
                $("#messageSpan").text(result.data);
            };
            ws.onerror = function (error) {
                $("#messageSpan").text(error.data);
            };
            ws.onclose = function () {
                $("#messageSpan").text("Disconnected!");
            };
        });
        $("#btnSend").click(function () {
            if (ws.readyState == WebSocket.OPEN) {
                ws.send($("#txtInput").val());
            } else {
                $("messageSpan").text("Connection is Closed!");
            }
        });
        $("#btnDisconnect").click(function () {
            ws.close();
        });
    }
);